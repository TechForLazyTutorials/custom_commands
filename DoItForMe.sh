#     DoItForMe.sh to automte the intial hurdles for adding custom linux
#     commands
#     Copyright (C) 2018  ICFOSS
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
# /*******************************************************************************
#  * DoItForMe.sh
#  *******************
#   This shell script in intended to auto add shell commands to .basrc and help the
#   user to make their custom commands.
#  *********************************************************************************

#Resize the window
printf '\e[8;19;122t'
echo -e "\nResizing the window to 122x19 for better ASCII art experiance.\n"
tput setaf 2 #set colour to green
echo "/%%%%%%%%                  /%%             /%%%%%%%%                        /%%                                     "
echo "|__  %%__/                 | %%            | %%_____/                       | %%                                    "
echo "   | %%  /%%%%%%   /%%%%%%%| %%%%%%%       | %%     /%%%%%%   /%%%%%%       | %%        /%%%%%%  /%%%%%%%% /%%   /%%"
echo "   | %% /%%__  %% /%%_____/| %%__  %%      | %%%%% /%%__  %% /%%__  %%      | %%       |____  %%|____ /%%/| %%  | %%"
echo "   | %%| %%%%%%%%| %%      | %%  \ %%      | %%__/| %%  \ %%| %%  \__/      | %%        /%%%%%%%   /%%%%/ | %%  | %%"
echo "   | %%| %%_____/| %%      | %%  | %%      | %%   | %%  | %%| %%            | %%       /%%__  %%  /%%__/  | %%  | %%"
echo "   | %%|  %%%%%%%|  %%%%%%%| %%  | %%      | %%   |  %%%%%%/| %%            | %%%%%%%%|  %%%%%%% /%%%%%%%%|  %%%%%%%"
echo "   |__/ \_______/ \_______/|__/  |__/      |__/    \______/ |__/            |________/ \_______/|________/ \____  %%"
echo "                                                                                                           /%%  | %%"
echo "                                                                                                          |  %%%%%%/"
echo "                                                                                                           \______/ "
tput sgr0 



echo -e "Hi, Tech for Lazy Here.\nShall I add a sample template of custom commands to your bashrc?"
echo "You can then edit the template for making your custom commands. "
tput setaf 1 #set colour to red
read -p 'SHALL I START? (Y/N) ' response
tput sgr0    #reset colour
response=${response,,} # tolower

if [[ $response =~ ^(yes|y| ) ]] || [[ -z $response ]];
then
    #to create a myCommands.sh which would act as source for terminal commands
    tput setaf 3 #set colour to yellow
    echo "Creating myCommands.sh in directory $PWD"
    tput sgr0    #reset colour
    touch $PWD/myCommands.sh
    printf '%s\n' '#!/bin/sh' 'echo "Sample Greeting, edit it by typing addnewcommands"' \
        '# You can comment this greeting by adding a # before the echo' '' \
        'function addnewcommands(){' \
        'echo "Open Text Editor To Add New Commands"' \
        'gedit '"$PWD"'/myCommands.sh' \
        '}' \
        'function mycommands(){' \
        'echo '"$PWD"'/myCommands.sh' \
        '}'\
        'function greetme(){' \
        'if [ "$#" == "0" ]' \
        'then' \
        'echo "Please enter you name as parameter"' \
        'return' \
        'fi' \
        'echo "Hai $1, How are you?"' \
        '}' \
        'function sampleFunctionName(){' \
        'echo "This is sample Function 1"'\
        '}'\
        'function sampleFunctionName2(){' \
        'echo "This is sample Function 2"'\
        '}'  > $PWD/myCommands.sh


    #to add the the new shell script file into .bashrc as a source file
    tput setaf 1 #set colour to red
    read -p 'Should i add myCommands.sh to .bashrc (y/n)?: ' response
    tput sgr0    #reset colour
    response=${response,,} # tolower
    if [[ $response =~ ^(yes|y| ) ]] || [[ -z $response ]];
    then
        echo "Linking myCommands.sh shell script file into .bashrc"
        echo "#Adding shell script for your CustomCommands"  >> ~/.bashrc
        echo "source $PWD/myCommands.sh" >> ~/.bashrc
        echo "File added to ~/.bashrc, please start a new terminal for loading the new set of commands."  
    else
        echo "Sample template > mycommands.sh created, file not linked with ~/.bashrc, please link it manually."
    fi
else
    echo "Exiting......"
fi

