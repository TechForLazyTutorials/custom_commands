Custom Terminal Commands
=========================
<img src="https://assets.gitlab-static.net/uploads/-/system/group/avatar/6544397/T4LLogo.png" width="30%">

Hi Guys, [Tech for Lazy](https://www.youtube.com/channel/UCcREtNSgoN6a7FgxmuE7Sqg) Here.

This repo will help the lazy you to skip the initial 
hurdles on linking your custom commands with bashrc so that
you will get to create your own set of terminal commands.

## Mode 
- [Lazy Mode](#lazy)
- [Learner Mode](#learner)

# Lazy 
- Download the [zip](https://gitlab.com/TechForLazyTutorials/custom_commands/-/archive/master/custom_commands-master.zip) or clone the repo.
- Unzip *(or move)* the folder in your desired location
(*Make sure that it is somewhere that you wouldn't delete or move from*)
- Give execution permision for DoItForMe.sh
```
cd /path/to/unzipped folder/
chmod +x DoItForMe.sh
```
- Run the shell DoItForMe.sh, and let it do the rest.
```
./DoItForMe.sh
```
> DoItForMe.sh, will create a template shell script myCommands.sh and will link it to .bashrc.
- Now open a new terminal and enter addnewcommands to edit 
the template file linked with the .bashrc.

# Learner
- Create a shell script with different commands as function
*(refer the below given example)*
```
#!/bin/sh

echo "Sample Greeting, edit it by typing addnewcommands"
# You can comment this greeting by adding a # before the echo

function addnewcommands(){
  echo "Open Text Editor To Add New Commands"
  gedit /home/vai/work/tutorials/custom_commands/myCommands.sh
}

function mycommands(){
  echo /home/vai/work/tutorials/custom_commands/myCommands.sh
}

function greetme(){
  if [ "$#" == "0" ]
    then
      echo "Please enter you name as parameter"
      exit
  fi
  echo "Hai $1, How are you?"     
}

function sampleFunctionName(){
  echo "This is sample Function 1"
}

function sampleFunctionName2(){
   echo "This is sample Function 2"
}
```
> Here the functions names will be your commands. 

- Get the location of your shell script.
- Now source the file in bashrc so that, our list functions
will be considered as commands from the next terminal 
session onwards 
```
gedit ~/.bashrc
```
> You can use any text editor you like, I am using gedit here.
- Add this line to your bashrc.
```
# Adding custom commands
source /path/to/your/shellScipt.sh
```
- Now open a new terminal session for loading your new set
of commands. 